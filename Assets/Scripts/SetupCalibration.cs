﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Menu { calib_start, calib_end, calib_rotation, calib_flexion};

public class Param_menu
{
	public Menu sel_menu;
	public float during;

	public Param_menu(Menu _menu, float _time)
	{
		sel_menu = _menu;
		during = _time;
	}
}

public class Pram_calib_result
{
	public Vector2 range_rotation;
	public Vector2 range_flexion;
	public Vector3 range_sideflexion;

	public Pram_calib_result()
	{
		range_rotation = new Vector2(0f, 1f);
		range_flexion = new Vector2(0f, 0.5f);
		range_sideflexion = new Vector2(0f, 1f);
	}
	public Pram_calib_result(Vector2 _range_rotation, Vector2 _range_flexion, Vector3 _range_sideflexion)
	{
		range_rotation = _range_rotation;
		range_flexion = _range_flexion;
		range_sideflexion = _range_sideflexion;
	}
}
public class SetupCalibration : MonoBehaviour {

	public GameObject messageCanvas;
	public GameObject mainCamera;
	public Vector2 range_rotation = new Vector2();
	public Vector2 range_flexion = new Vector2();
	public Vector2 range_sideFlexion = new Vector2();
	// Use this for initialization
	void Start () {

		//messageCanvas = GameObject.Find ("MyMessageCanvas");	
		messageCanvas = transform.Find("MyMessageCanvas").gameObject;
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void StartCalibration()
	{
		StartCoroutine ("coroutine_calibration");
	}
	IEnumerator coroutine_calibration()
	{
		float during = 5f;
		float startTime = Time.time;

		StartCoroutine (ShowMenu_for_seconds (new Param_menu (Menu.calib_start, during)));
		while (Time.time - startTime < during)  {
			yield return new WaitForFixedUpdate ();
		}

		during = 15f;
		startTime = Time.time;
		StartCoroutine (ShowMenu_for_seconds (new Param_menu (Menu.calib_rotation, during)));

		while (Time.time - startTime < during) {
			yield return new WaitForFixedUpdate ();
		}

		during = 15f;
		startTime = Time.time;
		StartCoroutine (ShowMenu_for_seconds (new Param_menu (Menu.calib_flexion, during)));
		while (Time.time - startTime < during) {
			yield return new WaitForFixedUpdate ();
		}
		during = 5f;
		startTime = Time.time;
		StartCoroutine (ShowMenu_for_seconds (new Param_menu (Menu.calib_end, during)));
		while (Time.time - startTime < during) {
			yield return new WaitForFixedUpdate ();
		}
	}

	string ComposeAngleText(Direction direction)
	{
		string str = "";
		switch (direction) 
		{
		case Direction.rotation:
			str =  "Min : " + range_rotation.x.ToString () + ", Max : " + range_rotation.y.ToString ();
			break;
		case Direction.Flexion:
			str =  "Min : " + range_flexion.x.ToString () + ", Max : " + range_flexion.y.ToString ();
			break;
		}
		return str;
	}

	void UpdateMinMax(Direction dir)
	{
		float currValue;
		Vector3 axis;

//		Debug.Log (mainCamera.transform.localRotation.eulerAngles);

		switch (dir) 
		{
		case Direction.rotation:
			
			axis = Vector3.up;
			currValue = mainCamera.transform.localRotation.eulerAngles.y;

			if (currValue >= 0 && currValue < 90 && currValue > range_rotation.y) {
				range_rotation.x = -currValue;
				range_rotation.y = currValue;
			}
			else if(currValue >=270 && currValue<360 && (currValue-360) < range_rotation.x){
				range_rotation.x = (currValue - 360f);
				range_rotation.y = -(currValue - 360f);
			}
					
			break;
		case Direction.Flexion:
			
			//mainCamera.transform.localRotation.ToAngleAxis (out currValue, out axis);
			//Debug.Log ("left" + currValue.ToString ());
			currValue =mainCamera.transform.localRotation.eulerAngles.x;

			if (currValue >= 0 && currValue < 90 && -currValue < range_flexion.x) {
				range_flexion.x = -currValue;
			}
			break;
		
		}
	}
	IEnumerator ShowMenu_for_seconds(Param_menu param)
	{
		float startTime = Time.time;

		messageCanvas.SetActive (true);
		Sprite chageImage;

		string angleText;
		switch (param.sel_menu) {
		case Menu.calib_start:
			messageCanvas.SendMessage ("ChangeText", "Start the calibration after 5 seconds.");
			messageCanvas.SendMessage ("ChangeAngleText", "");
			messageCanvas.SendMessage ("ChangeVisibleState", false);
			while (Time.time - startTime < param.during) {
				yield return new WaitForFixedUpdate ();
			}
			break;
		case Menu.calib_end:
			messageCanvas.SendMessage ("ChangeText", "Start the NexerciseVR after 5 seconds.");
			messageCanvas.SendMessage ("ChangeAngleText", "");
			messageCanvas.SendMessage ("ChangeVisibleState", false);
			while (Time.time - startTime < param.during) {
				yield return new WaitForFixedUpdate ();
			}
			break;
		case Menu.calib_rotation:
			messageCanvas.SendMessage ("ChangeText", "Move your head to the left and right as far as you can.");
			chageImage= Resources.Load <Sprite> ("Sprites/rotation");
			messageCanvas.SendMessage ("ChangeSprite", chageImage);
			messageCanvas.SendMessage ("ChangeVisibleState", true);

			while (Time.time - startTime < param.during) 
			{
				UpdateMinMax (Direction.rotation);
				angleText = ComposeAngleText (Direction.rotation);
				messageCanvas.SendMessage ("ChangeAngleText", angleText);
				yield return new WaitForFixedUpdate ();
			}
					
			break;
		case Menu.calib_flexion:
			messageCanvas.SendMessage ("ChangeText", "Move your head down as far as you can.");
			chageImage= Resources.Load <Sprite> ("Sprites/flexion");
			messageCanvas.SendMessage ("ChangeSprite", chageImage);
			messageCanvas.SendMessage ("ChangeVisibleState", true);

			while (Time.time - startTime < param.during) 
			{
				UpdateMinMax (Direction.Flexion);
				angleText = ComposeAngleText (Direction.Flexion);
				messageCanvas.SendMessage ("ChangeAngleText", angleText);
				yield return new WaitForFixedUpdate ();
			}
			break;
		}

		messageCanvas.SetActive (false);
	}
	public Pram_calib_result GetResult()
	{
		
		range_rotation.x = (range_rotation.x + 90.0f)/180f;
		range_rotation.y = (range_rotation.y + 90.0f)/180f;

		range_flexion.x = (range_flexion.x + 90.0f)/180f;
		range_flexion.y = (range_flexion.y + 90.0f)/180f; //must be 0.5f

		range_sideFlexion.x = (range_sideFlexion.x + 90.0f)/180f;
		range_sideFlexion.y = (range_sideFlexion.y + 90.0f)/180f;

		//Debug.Log ("Calib result rotation : " + range_rotation.ToString ());
		//Debug.Log ("Calib result flexion : " + range_flexion.ToString ());

		return new Pram_calib_result (range_rotation, range_flexion, range_sideFlexion);
	}
}
