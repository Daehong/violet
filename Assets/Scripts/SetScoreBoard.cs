﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SetScoreBoard : MonoBehaviour {

	public int score = 0;
	UnityEngine.UI.Text textScore;
	// Use this for initialization
	void Start () {
		textScore = GetComponent<UnityEngine.UI.Text> ();
		textScore.text = score.ToString ();
	}

	
	// Update is called once per frame
	void Update () {
		
	}
	void AddScore()
	{
		score++;
		textScore.text = score.ToString ();
		
	}
}
