﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedureManager : MonoBehaviour {

	GameObject myScoreBoardCanvas;
	Vector2 range_rotation = new Vector2 ();
	Vector2 range_flexion = new Vector2 ();
	Vector2 range_sideflexion = new Vector2();
	GvrAudioSource audioSource;

	bool bCalibOK = false;
	public bool bAutoStart = false;

	// Use this for initialization
	void Start () {
		audioSource = GameObject.Find("Audio Source").GetComponent<GvrAudioSource> ();
		myScoreBoardCanvas = transform.Find ("ScoreBoardCanvas").gameObject;

		if (bAutoStart) 
		{
			StartCoroutine ("StartNexiersizeVR");
		}
	}
	IEnumerator StartNexiersizeVR()
	{
		float startTime = Time.time;

		audioSource.clip = Resources.Load<AudioClip> ("Healing Music3_short_uncompressed");
		audioSource.Play ();
		gameObject.SendMessage ("StartCalibration");
		bCalibOK = true;

		while (Time.time - startTime < 40) {
			yield return new WaitForFixedUpdate ();
		}

		audioSource.Stop ();
		myScoreBoardCanvas.SetActive (true);
		audioSource.clip = Resources.Load<AudioClip> ("Healing Music1_short_uncompressed");

		Pram_calib_result calib_result;
		if(bCalibOK)
			calib_result = GetComponent<SetupCalibration> ().GetResult ();
		else
			calib_result = new Pram_calib_result();

		gameObject.SendMessage ("Generate_and_Run_Notes", calib_result);


	}
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit(); 

		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{

			audioSource.clip = Resources.Load<AudioClip> ("Healing Music3_short_uncompressed");
			audioSource.Play ();
			gameObject.SendMessage ("StartCalibration");
			bCalibOK = true;
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			audioSource.Stop ();
			myScoreBoardCanvas.SetActive (true);
			audioSource.clip = Resources.Load<AudioClip> ("Healing Music1_short_uncompressed");

			Pram_calib_result calib_result;
			if(bCalibOK)
				calib_result = GetComponent<SetupCalibration> ().GetResult ();
			else
				calib_result = new Pram_calib_result();
			
			gameObject.SendMessage ("Generate_and_Run_Notes", calib_result);
		}
	}


}
