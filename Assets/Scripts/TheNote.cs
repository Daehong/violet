﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Direction { rotation,Flexion, sideFlexion, chinTucks, none};
public class TheNote : MonoBehaviour
{
    public string name_notePrefab = "Prefabs/Note_default";
    public string name_guidePrefab = "Prefabs/Note_transparent";
    
    public Direction direction = Direction.rotation;
    public float distance_from_camera = 5f;
    public float during_total;
	public float during_timeSeries;
    public float startTime = 0f;
    public Vector2 range = new Vector2(0f, 0.5f);

    public float cubeSize = 0.5f;
    public float[] timeSeries = { 0.5f, 1.5f, 2.5f, 3.1f, 4.2f };

    void Start()
    {
		Invoke ("ShowGuideBoard", startTime - 5f);
        Invoke("myCoroutin", startTime);
    }

    void myCoroutin()
    {
        StartCoroutine(moveObject());
    }

	void ShowGuideBoard()
	{
		GameObject guideBoard = transform.Find ("GuideBoardCanvas").gameObject;
		guideBoard.SetActive (true);
		guideBoard.SendMessage ("StartCountDown", 5f);

		Sprite changeImage;

		if(direction == Direction.rotation && range.y < 0.5f) // rotation - left
			changeImage = Resources.Load <Sprite> ("Sprites/rotation_left");
		
		else if(direction == Direction.rotation && range.y >= 0.5f) // rotation - left
			changeImage = Resources.Load <Sprite> ("Sprites/rotation_right");

		else if(direction == Direction.Flexion && range.y < 0.5f) // flexion - down
			changeImage = Resources.Load <Sprite> ("Sprites/flexion_down");

		else if(direction == Direction.sideFlexion && range.y < 0.5f) // sideFlexion - left
			changeImage = Resources.Load <Sprite> ("Sprites/sideflexion_left");

		else 
			changeImage = Resources.Load <Sprite> ("Sprites/sideflexion_right");
		
		guideBoard.SendMessage ("ChangeSprite", changeImage);

	}



	public void SetParametersUsingTimeSeries(float[] input_timeSeries, Direction _direction, Vector2 _range, float startTime_audio, float margin_from_first_note)
	{
		//margin_from_first_note = _margin_from_first_note;
		timeSeries = new float[input_timeSeries.Length];

		for (int i = 0; i < input_timeSeries.Length; i++)
			timeSeries [i] = input_timeSeries [i] - input_timeSeries [0];

		during_timeSeries = input_timeSeries [input_timeSeries.Length - 1] - input_timeSeries [0];
	
		startTime = input_timeSeries [0] - margin_from_first_note - startTime_audio;
		during_total = during_timeSeries + margin_from_first_note*2;

		direction = _direction;

		if (_direction == Direction.sideFlexion) 
		{
			distance_from_camera = 0.3f;
			cubeSize *= (0.3f / 5.0f);
		}
		range = _range;
	}

    Vector3 GetPositionInPath(Direction direction, float timNorm)
    {
        Vector3 targetPosition = new Vector3();
        float theta_start = Mathf.PI * (1 - range.x);

        float theta_end = Mathf.PI * (1 - range.y);
        float theta = Mathf.Lerp(theta_start, theta_end, timNorm);

        switch (direction)
        {
            case Direction.rotation:

                targetPosition.x = distance_from_camera * Mathf.Cos(theta);
                targetPosition.y = 0;
                targetPosition.z = distance_from_camera * Mathf.Sin(theta);
                break;


            case Direction.Flexion:

                targetPosition.x = 0;
                targetPosition.y = distance_from_camera * Mathf.Cos(theta);
                targetPosition.z = distance_from_camera * Mathf.Sin(theta);
                break;

            case Direction.sideFlexion:

                targetPosition.x = distance_from_camera * Mathf.Cos(theta);
				targetPosition.y = distance_from_camera * Mathf.Sin(theta) - distance_from_camera;
                targetPosition.z = 1;
                break;

            case Direction.chinTucks:
                break;
        }

       
        return targetPosition;

    }

    IEnumerator moveObject()
    {
        float initTime = Time.time;
        float movingDistance = Mathf.PI * (range.x - range.y);

        //GameObject testObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject testObj = Instantiate(Resources.Load(name_notePrefab, typeof(GameObject))) as GameObject;
        testObj.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);
		float spentTime = 0;
              
        GameObject[] guidObject = new GameObject[timeSeries.Length];
        //float during_timeSeries = timeSeries[timeSeries.Length - 1] - timeSeries[0];

        //create guide object
        for (int i=0; i<timeSeries.Length; i++)
        {
            guidObject[i] = Instantiate(Resources.Load(name_guidePrefab, typeof(GameObject))) as GameObject;
            guidObject[i].transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

			if(during_timeSeries!=0)
				guidObject[i].transform.position = GetPositionInPath(direction, timeSeries[i] / during_timeSeries);
			
			else
				guidObject[i].transform.position = GetPositionInPath(direction,0);
				
        }

        //move target obj
        while (spentTime <= during_total)
        {
			spentTime = Time.time - initTime; 

			if(during_timeSeries!=0)
            	testObj.transform.position = GetPositionInPath(direction, spentTime / during_total);
			else
				testObj.transform.position = GetPositionInPath(direction, 0);
           

            yield return new WaitForFixedUpdate();
        }
        Destroy(testObj);
        for(int i=0; i<timeSeries.Length; i++)
        {
            Destroy(guidObject[i]);
        }
        this.enabled = false;
		//Debug.Log("Spent Time : " + spentTime.ToString() + " During Time : " + during_total.ToString());
		//Destroy (this);
    }

}
