using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventGazed : MonoBehaviour {

	public EventGazed targetObj;
	public GameObject scoreBoard;

	public Material inactiveMaterial;
	public Material gazedAtMaterial;
	public bool bGazeState = false;

	// Use this for initialization
	void Start () {
		targetObj = GameObject.FindGameObjectWithTag ("TargetObject").GetComponent<EventGazed>();
		scoreBoard = GameObject.FindGameObjectWithTag ("ScoreBoard");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void SetGazedAt(bool gazedAt) {
		bGazeState = gazedAt;

		if (inactiveMaterial != null && gazedAtMaterial != null) {
			//Debug.Log ("Gazed");
			GetComponent<Renderer>().material = gazedAt ? gazedAtMaterial : inactiveMaterial;
			return;
		}
		GetComponent<Renderer>().material.color = gazedAt ? Color.green : Color.red;
	}

	public void CheckPoint()
	{
		if (targetObj != null && targetObj.bGazeState == bGazeState) {
			scoreBoard.SendMessage ("AddScore");
		}

		Destroy (gameObject);
			//Debug.Log ("Add Score");
		
		

	}



}
