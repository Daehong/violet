﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Move : MonoBehaviour
{

    public int countTick = 0;

    public float speed;
    public Text countText;
    public Text totalText;
    public Text rightText;
    //public Text missText;

    private int totalCount;
    private int rightCount;
    //private int missCount;
    private int totalScore;
    public AudioSource BGM;
    // just for manual time management
    private float timer;

    public GameObject Quad;
    public GameObject Line;
    public GameObject Note;
    public GameObject Ground;
    public GameObject Bonus;
    public GameObject Marker;
    static int Bai = 15; //BAIsoku 즉 배속 
    //     TextMesh tm;
    void Start()
    {
        rightCount = 0;
        //missCount = 0;
        SetCountText();
        totalText.text = "";
        rightText.text = "";
        //missText.text = "";
        BGM.PlayDelayed(3);
        timer = 0.0f;
     //   tm = gameObject.GetComponentInChildren<TextMesh>() as TextMesh;
    }

    void FixedUpdate()
    {
        
        timer += Time.deltaTime;
        // manual time management (game starts after 3 sec)
        if (timer > 3.0f)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            //float moveVertical = Input.GetAxis("Vertical");

            // Rotation of a car
            //(not necessary, constant -- 2.0f here -- is arbitrary)
            transform.Rotate(
                0.0f,
                2.0f * speed * moveHorizontal * Time.deltaTime,
                0.0f);

            // Movement of a car (constant -- 0.5f here -- is arbitrary)
            transform.Translate(
                0.5f * speed * moveHorizontal * Time.deltaTime,
                0.0f,
                speed*Bai*0.1f* Time.deltaTime);
            if (transform.position.z > (Bai * countTick))
            {
                GameObject.Instantiate(Quad, new Vector3(0, 0.1f, (Bai * countTick + 9 * Bai)), Quaternion.identity);
                GameObject.Instantiate(Line, new Vector3(0, 0.1f, (Bai * countTick + 9 * Bai)), Quaternion.identity);

                GameObject.Instantiate(Quad, new Vector3(-15, 10, (Bai * countTick + 9 * Bai)), Quaternion.Euler(0,0,-90));
                GameObject.Instantiate(Line, new Vector3(-15, 10, (Bai * countTick + 9 * Bai)), Quaternion.Euler(0,0,-90));

                //GameObject.Instantiate(Ground, new Vector3(0, 0, (Bai * countTick + 9 * Bai)), Quaternion.identity);
                GameObject.Instantiate(Marker, new Vector3(-4, 2, (Bai * countTick + 5 * Bai)), Quaternion.identity);
                GameObject.Instantiate(Marker, new Vector3(4, 2, (Bai * countTick + 5 * Bai)), Quaternion.identity);


                //Note block generator
                //Create a note table for deom
                GameObject.Instantiate(Note, new Vector3((-3+(Random.Range(0,4))*2), 0.2f, (Bai * countTick + 9*Bai)), Quaternion.identity);
                countTick++;
                if (countTick % 16 == 13)
                {
                    GameObject.Instantiate(Bonus, new Vector3(-30 + (Random.Range(0, 2) * 60), 7, Bai * countTick + 9 * Bai), Quaternion.identity);
                }

            }
            // needs a judging system (maybe)
            //     tm.text = "Current Beat: "+countTick.ToString();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))  // Purple notes
        {
            other.gameObject.SetActive(false);
            rightCount++;
            totalScore += 10000;
            SetCountText();
        }

        else if (other.gameObject.CompareTag("Double Pick"))  // Yellow (multiple) notes
        {
            other.gameObject.SetActive(false);
            rightCount++;
            totalScore += 15000;
            SetCountText();
        }

        // still do not have triggers on misses
    }

    void SetCountText()
    {
        countText.text = totalScore.ToString().PadLeft(6, '0');  // PadLeft: make a 6 digit number
    }
}