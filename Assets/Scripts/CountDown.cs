﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDown : MonoBehaviour {
	// Use this for initialization
	void Start () {
		StartCountDown (5.3f);
	}

	void StartCountDown(float during)
	{
		StartCoroutine (CountDownStart (during));
	}
	IEnumerator CountDownStart(float during)
	{
		float startTime = Time.time;

		while (Time.time - startTime < during) 
		{
			float remain = during - (Time.time - startTime);
			//if(remain)
			{
				string str = string.Format ("Next action ({0:0.#} sec)", remain);
				gameObject.SendMessage ("ChangeText", str);
			}
			yield return new WaitForFixedUpdate ();
		}

		gameObject.SetActive (false);
	}
}
