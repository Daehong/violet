﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;



public class AutoNoteGenerator : MonoBehaviour {

	//public AudioSource audioSource;
	public GvrAudioSource audioSource;
	public float startTime_audio = 0f;
	public float endTime_audio = 300f;
    public string filename_onset_txt;
    public float[] onset_list = null;
	public List<float[]> onset_groups;
	public float distance_b2w_groups = 4f;
	public float margin_from_first_note = 0f;
	public float maxTime = 4.0f;
	float margin_from_boundary = 0f;

	private bool bLoadOnsets = false;
	Pram_calib_result calib_result;
    // Use this for initialization
    void Start () {


		audioSource = GameObject.Find("Audio Source").GetComponent<GvrAudioSource> ();

        
		//Debug.Log("the number of groups : "+ onset_groups.Count.ToString());
		//Generate_and_Run_Notes();

    }
	void Update()
	{
		if (bLoadOnsets && audioSource.time >= endTime_audio) 
		{
			audioSource.Stop ();
		}
		
	}
	void Generate_and_Run_Notes(Pram_calib_result _calib_result)
	{
		calib_result = _calib_result;
		LoadOnsetList();
		onset_groups = ClusterOnsetsbyDistance(distance_b2w_groups);


		audioSource.Play ();
		audioSource.time = startTime_audio;

		for (int i = 0; i < onset_groups.Count; i++) 
		{
			//filter unnessasery(or warong) onset groups.
			if (onset_groups [i][0] >= (startTime_audio + margin_from_first_note) 
				&& onset_groups[i][onset_groups[i].Length-1] < endTime_audio
				&& onset_groups[i].Length > 1) 

			{
				float length = onset_groups [i] [onset_groups [i].Length - 1] - onset_groups [i] [0];
				float ratio = length / maxTime;

				Direction dir = GetRandomDirection ();
				Vector2 range = GetRandomRange (dir);
				TheNote newNote;

				if (ratio < 1.0f)
					range = ShortenRange (range, ratio);
				
				newNote = gameObject.AddComponent<TheNote> ();
				newNote.SetParametersUsingTimeSeries (onset_groups [i], dir, range, startTime_audio, margin_from_first_note);

		
				//List<float[]> subgroups = seperate_onset_group (onset_groups [i]);

				//int count = (int)ratio;
				/*
				for(int j=0; j<subgroups.Count; j++)
				{
					newNote = gameObject.AddComponent<TheNote> ();
					newNote.SetParametersUsingTimeSeries (subgroups[j], dir, range, startTime_audio, margin_from_first_note);
					Vector2 newRange = TrunDirection (range);
					range = newRange;
				}

				float spare_length = ratio - Mathf.Floor (ratio);

				if (spare_length > 0.0f) {
					range = ShortenRange (dir, range, spare_length);
					newNote = gameObject.AddComponent<TheNote> ();
					newNote.SetParametersUsingTimeSeries (subgroups [count], dir, range, startTime_audio, margin_from_first_note);
				}
				*/


			}
		}

	}
	List<float[]> seperate_onset_group(float[] onset_group)
	{
		float totallength = onset_group [onset_group.Length - 1] - onset_group [0];
		float count = totallength / maxTime;
		List<float[]> subgroups = new List<float[]> ();

		for (int i = 0; i < count; i++) 
		{
			
		}
		//float startTime_onset_group = onset_group [0];

		List<float> sub = new List<float> ();
		sub.Add (onset_group [0]);
		/*
		for (int i = 0; i < onset_group.Length; i++) 
		{
			if (onset_group [i] - startTime_onset_group <= maxTime)
				sub.Add (onset_group [i]);
			
			else 
			{
				float[] newGroup = new float[sub.Count];
				sub.CopyTo (newGroup);
				subgroups.Add (newGroup);
				sub.Clear();
				sub.Add (onset_group [i]);
				startTime_onset_group = onset_group [i];
			}
		}*/

		return subgroups;
	}
	Vector3 ShortenRange(Vector2 range, float ratio)
	{
		
		range = (range - new Vector2 (0.5f, 0.5f)) * ratio + new Vector2 (0.5f, 0.5f);

		return range;
	}
	Vector3 TrunDirection(Vector2 range)
	{
		return new Vector2 (range.y, range.x);
	}
		
	Direction GetRandomDirection()
	{
		Direction dir = new Direction();
		int rnum = Random.Range (0, 2);
		switch (rnum) {
		case 0:
			dir = Direction.rotation;	

			break;
		case 1:
			dir = Direction.Flexion;
	
			break;
		case 2:
			dir = Direction.sideFlexion;
	
			break;
		}
		return dir;

	}
	Vector2 GetRandomRange(Direction dir)
	{
		int rnum = Random.Range (0, 2);
		Vector2 range = new Vector2 ();
		switch (dir) {
		case Direction.rotation:
			if (rnum == 0) {
				range = new Vector2 (0.5f, calib_result.range_rotation.y - margin_from_boundary); //right
			} else {
				range = new Vector2 (0.5f, calib_result.range_rotation.x + margin_from_boundary); //left
			}

			break;
		case Direction.Flexion:
			range = new Vector2 (0.5f, calib_result.range_flexion.x + margin_from_boundary); //down

			break;
		case Direction.sideFlexion:
			if (rnum == 0) {
				range = new Vector2 (0.5f, calib_result.range_sideflexion.y - margin_from_boundary); //right
			} else {
				range = new Vector2 (0.5f, calib_result.range_sideflexion.x + margin_from_boundary);//left
			}
			break;
		}
		return range;

	}
	void LoadOnsetList()
    {
        string filepath = "OnsetDetector/";

		//if (!Directory.Exists(Application.dataPath + filepath))
            //Debug.LogError("Warong onset filepath.");

        //else
        {
			TextAsset bindata = (TextAsset) Resources.Load (filepath+filename_onset_txt, typeof(TextAsset));

			//StreamReader sr = new StreamReader(bindata.text);
            //string[] buffer = sr.ReadLine().Split(',');
			string[] buffer = bindata.text.Split(',');
            onset_list = new float[buffer.Length];
            for (int i=0;i<buffer.Length; i++)
            {
                onset_list[i] = float.Parse(buffer[i])/100.0f;
            }

            bLoadOnsets = true;
        }


    }

    List<float[]> ClusterOnsetsbyDistance(float minDistance)
    {
		List<float[]> onset_groups = new List<float[]>();

        List<float> newGroup = new List<float>();
        newGroup.Add(onset_list[0]);

        for (int i=1; i<onset_list.Length; i++)
        {
			if( (onset_list[i] - onset_list[i-1]) < minDistance)
            {
                newGroup.Add(onset_list[i]);
            }
            else
            {
				float[] nGroup = new float[newGroup.Count];
				newGroup.CopyTo (nGroup);
				onset_groups.Add(nGroup);
                newGroup.Clear();
				newGroup.Add(onset_list[i]);
            }
        }

        return onset_groups;
    }



}
