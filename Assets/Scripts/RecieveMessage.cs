﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class RecieveMessage : MonoBehaviour {

	public Text myText;
	public Image myImage;
	public Text angleText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void ChangeText(string strText)
	{
		myText = transform.Find("MessageText").GetComponent<Text>(); 
		myText.text = strText;
	}

	void ChangeAngleText(string strText)
	{
		angleText = transform.Find("AngleText").GetComponent<Text>(); 
		angleText.text = strText;
	}

	void ChangeSprite(Sprite toImage)
	{
		myImage = transform.Find ("Picture").GetComponent<Image> ();
		myImage.sprite = toImage;
	}

	void ChangeVisibleState(bool isVisible)
	{
		myImage = transform.Find ("Picture").GetComponent<Image> ();	
		if(isVisible)
			myImage.color = new Color (1f, 1f, 1f, 1f);
		else
			myImage.color = new Color (1f, 1f, 1f, 0f);
	}
}
