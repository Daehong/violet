function  [onsets, peakFluxVals,xf_mag_comp]=onsetDetection(x_mag,params)

%% onset detection algorithm 
x_pow_db = 10*log10(mean(x_mag.^2,1));

C= 10; %15 
xf_mag_comp = log10(1+C*x_mag);
odf = spec_flux(xf_mag_comp, 10);


[onsets, odf_max, odf_mean, power_min] = onset_peak_picking(odf, x_pow_db, params);
% convOnsets=onsets;
% [refinedOnsets, onsetEnergy] = findOnsetRmsEnergy(s, onsets, sEnergyThres,sEnergyFrameMargin);


%% adjust refined onset
peakFluxVals=zeros(1,size(x_mag,2)-1); 
% onsets=refinedOnsets;
for i=1:length(onsets)
    peakFluxVals( onsets(i) ) = 1;
end

end
