function y = spec_flux(x, mu)

% x: magnitude spectrogram (compressed)
% mu: time difference
% y: spectral flux 

x_diff = x(:,[mu+1:end])-x(:,[1:end-mu]);
y = sum(max(x_diff,0),1);
y = [zeros(1,mu) y];
