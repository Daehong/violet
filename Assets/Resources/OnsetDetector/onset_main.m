%%
params.fft = struct('win_size', 2048, ...
                    'nfft', 2048, ... 
                    'hop_size', 441 ,...
                    'noverlap', 2048-441, ... #win_size - hop_size
                    'fs', 44100);
                
params.onset = struct('mu', 2, ...
      'w1', 1, 'w2', 0, 'w3', 2, 'w4', 10,'w5', 20, ... 
      'thres', 50 , ...
      'min_power', 2); %23dB

filepath = 'C:/co/NexersizeVR_integrated/Assets/Resources/';
filename = 'Healing Music3_short_uncompressed.wav';
[y,paramsFs] = audioread([filepath filename]); 
% y = resample(y,paramsFs,params.fft.fs);

if size(y,2) == 2
    y = (y(:,1)+y(:,2))/2;
end

%-----------
%STFT
%-----------
window = hann(params.fft.win_size);
[S,f,t] = spectrogram(y,window, params.fft.noverlap, params.fft.nfft, params.fft.fs); 
S = abs(S);

%-----------
%onset detection
%-----------
[onsets,peakFluxVals,S]=onsetDetection(S,params);
%writetable(onsets, '/class_names.txt', 'WriteVariableNames', false);
%dlmwrite('for_test.txt', onsets, 'newline', 'pc');
[aa, name, cc] = fileparts(filename);
dlmwrite([name '.txt'], onsets, 'newline', 'pc');
figure()

hold on
imagesc(S);
ylim([0 , 200])
plot(peakFluxVals*200,'y','linewidth',2);
