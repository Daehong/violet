function [onsets, odf_max, odf_mean, power_min] = onset_peak_picking(odf, x_pow_db, params)

% odf: onset detection function
% options.onset
%   - w1, w2: previous and post window size for the maximum criterion
%   - w3, w4: previous and post window size the mean threshold criterion
%   - thres: othreshold for the the mean options.onset.threshold criterion
%   - w5: interonset-interval(IOI) criterion
% onsets: determined onset times
% odf_max: max criteria output (optional)
% odf_mean: mean criteria output (optional)
% define parameters for evaluation

% options.onset.onset_margin=250; % 250ms
% options.evaluation.freq_margin=0.1; % 10%

% step 1: max filter
odf_max = odf;

for i=1:length(odf)
    index1 = max(1, i-params.onset.w1);
    index2 = min(i+params.onset.w2, length(odf));    
    odf_max(i) = max(odf(index1:index2));    
end

% step 2: mean options.onset.thres
odf_mean = odf;

for i=1:length(odf)
    index1 = max(1, i-params.onset.w3);
    index2 = min(i+params.onset.w4, length(odf));    
    odf_mean(i) = mean(odf(index1:index2));    
end

% step 3: minimum_power
power_min = x_pow_db;

for i=1:length(x_pow_db)
%    index1 = max(1, i);
    index1 = min(i+1, length(x_pow_db));
    index2 = min(i+5, length(x_pow_db));    
    power_min(i) = min(x_pow_db(index1:index2));    
end

onset_candiates = find((odf_max == odf) & ...
                    (odf >= (odf_mean+params.onset.thres)) & ...
                    (power_min >= (min(x_pow_db)+params.onset.min_power)) ...                   
                  );


% step 4: last filtering
if isempty(onset_candiates)
    onsets = [];
else  
    onsets = onset_candiates(1);
    for i=1:length(onset_candiates)-1
        if (onset_candiates(i+1)-onset_candiates(i)) > params.onset.w5
            onsets = [onsets onset_candiates(i+1)];
        end
    end
end


